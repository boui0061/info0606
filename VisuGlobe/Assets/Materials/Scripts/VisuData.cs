using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisuData : MonoBehaviour
{
    public Transform Cible;
    public GameObject VisuCube;
    public float multiplier;
    
    public Material lowPopulationMaterial;
    public Material mediumPopulationMaterial;
    public Material highPopulationMaterial;

    Vector3 latloncart(float lat, float lon) 
    {
        Vector3 pos;
        float x = 0.5f * Mathf.Cos(lon) * Mathf.Cos(lat);
        float y = 0.5f * Mathf.Cos(lon) * Mathf.Sin(lat);
        float z = 0.5f * Mathf.Sin(lon);
        pos.x = 0.5f * Mathf.Cos((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad);
        pos.y = 0.5f * Mathf.Sin(lat * Mathf.Deg2Rad);
        pos.z = 0.5f * Mathf.Sin((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad);
        return pos;
    }

    public void VisualCube(float lat, float lon, float val, float multiplier)
    {
        GameObject cube = Instantiate(VisuCube);
        Vector3 pos = latloncart(lat, lon);
        cube.transform.position = new Vector3(pos.x, pos.y, pos.z);
        cube.transform.LookAt(Cible, Vector3.back);

        // Choix du matériau en fonction de la population
        Material chosenMaterial = lowPopulationMaterial; // Valeur par défaut
        if (val < 1000000) { // Supposons que < 1 million est faible
            chosenMaterial = lowPopulationMaterial;
        } else if (val < 10000000) { // Supposons que < 10 millions est moyen
            chosenMaterial = mediumPopulationMaterial;
        } else { // Supposons que >= 10 millions est élevé
            chosenMaterial = highPopulationMaterial;
        }

        // Appliquer le matériau choisi
        Renderer cubeRenderer;
        if (cube.GetComponent<Renderer>() != null) {
            cubeRenderer = cube.GetComponent<Renderer>();
        } else {
            // Assumer que le Renderer est sur le premier enfant si non trouvé sur l'objet parent
            cubeRenderer = cube.transform.GetChild(0).GetComponent<Renderer>();
        }
        cubeRenderer.material = chosenMaterial;

        // Définir le globe comme parent
        cube.transform.SetParent(GameObject.Find("Globe").transform);

        Vector3 scale = cube.transform.localScale;
        scale.z = val * multiplier;
        cube.transform.localScale = scale;
    }
}
