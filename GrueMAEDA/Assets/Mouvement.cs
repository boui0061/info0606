using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            // Déplace l'objet vers le bas sur l'axe y
            transform.Translate(Vector3.down * 0.05f);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            // Déplace l'objet vers le haut sur l'axe y
            transform.Translate(Vector3.up * 0.05f);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Rotation vers la gauche
            transform.Rotate(Vector3.forward, -2);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            // Rotation vers la droite
            transform.Rotate(Vector3.forward, 2);
        }
    }
}
