using UnityEngine;


//[RequireComponent(typeof(AudioSource))]
public class AudioVisu : MonoBehaviour
{
    public float[] spectrumH = new float[128];
    void Update()
    {
        AudioListener.GetSpectrumData(spectrumH, 0, FFTWindow.BlackmanHarris);
    }
}