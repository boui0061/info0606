using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaFonctionOnBeat : MonoBehaviour
{
    public AudioProcessor audioProcessor;
    public GameObject visualObject;

    private void Start()
    {
        if (audioProcessor != null)
        {
            audioProcessor.onBeat.AddListener(OnBeatDetected);
        }
    }

    private void OnBeatDetected()
    {
        if (visualObject != null)
        {
            float newSize = Random.Range(0.5f, 2f);
            visualObject.transform.localScale = new Vector3(newSize, newSize, newSize);
        }
    }
}