using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaFonctionOnSpectrum : MonoBehaviour
{
    public AudioProcessor audioProcessor;
    public GameObject visualObject;

    void Start()
    {
        if (audioProcessor != null)
        {
            audioProcessor.onSpectrum.AddListener(OnSpectrum);
        }
    }

    void OnSpectrum(float[] spectrum)
    {
        if (visualObject != null)
        {
            float maxIntensity = 0;
            foreach (float intensity in spectrum)
            {
                if (intensity > maxIntensity)
                {
                    maxIntensity = intensity;
                }
            }

            float scaledIntensity = maxIntensity * 100; // Facteur d'échelle
            scaledIntensity = Mathf.Clamp(scaledIntensity, 0, 1); // Limiter entre 0 et 1

            Renderer renderer = visualObject.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.material.color = new Color(scaledIntensity, 0, 0); // Rouge avec intensité variable
            }
        }
    }
}
